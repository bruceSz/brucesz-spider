# -*- coding:utf8 -*-
import json

from db import DatabaseAdmin


class Storer(object):
    """ Base class for all storer """

    def load(self,json_str):
        raise NotImplementedError

    def store(self):
        raise NotImplementedError
        



class JsonStorer(Storer):
    """ accept string of json and deal with translate it to a  instance
        also deal with store it into the db.
    """

    def load(self,json_str):
        self.object_ = json.loads(json_str)

    def store(self):
        print type(self.object_)


class CaifuJsonStorer(JsonStorer):
    """ baidu caifu specified json storer
        form of caifu data is like:
        {
            status: 0,
            data: {
                serverTime: 1395922946541,
                pageNum: 89,
                pageSize: 10,
                count: 882,
                signTime: 0,
                list: [],
                stats: {
                    subqid: 1395922946538633,
                    pssid: "0",
                    sid: "ui:0&bsInsurance:2&bsInvest:2"
                }
            }
        }
        the list contains target data in form of :
        {
            titleAll: "嘉实基金 - 嘉实理财宝7天债券A",
            id: 359051624,
            title: "嘉实基金 - 嘉实理财宝7天债券A",
            investCycle: "一个月以下",
            expectedProfitRate: "-27.33",
            expectedProfitName: "年净值增长率",
            lowestAmount: "100元",
            breakEven: 0,
            supplierLogoUrl: "http://t11.baidu.com/it/u=3962381380,1534452886&fm=58",
            rawUrl: "http://caifu.baidu.com/rcv?module=finance&u=010845fe4b9c6c87adbffc0fbf52f1216c300ad7e80c8cb2f36a10ae09dfc36d0ca704b7186ca0f9d8d80c1f140805327e594bfe7aedfce737eae2627447cbcf17824c632630ef29bbb24e8a6c27cee24c36cdc3933c0a8f74ecf4a46cd01&o=010845fe4b9cdd361c0ac8ae9c169270b9156f523ca99fe7c5efb27bb80af69bcc83d605698ca7b998ef0cd8f1387571cab8ea3b0fefbc3357da826727866a2db6720cc387346fcb9e568b9fb867ad327ea6ace20669280a206825a049407a5cda3362878920cae91cea02a48fac0635f605b37e81a314c958bdb95cf4806154f9b94aeca5420264c242a8871f8e0ff73bab12d83f76b63f11fe49f5cc7d22a31caa16eecf97788fea8dc61eee67122d1a45a071132d137bea4ee7f976cdda05654208fc174ef720ce73e9555cdfcef74c8f016f6b49c22dad55be43336dad3692e1e573b77f515115627f2a382c0da27b8f34f245f0f228985e4243cdefab32307c9e3b4fff6816ff6dbd5d43cc6b0672d69d6c9ab2e93f1d809f0c5466ec6e2",
            risk: "稳健型",
            profitType: 0,
            profitDesc: [
                {
                    name: "收益说明",
                    value: "不同产品的预期年化收益率的含义有所区别，产品托管费率和手续费请咨询具体发售机构。"
                }
            ],
            earlyBack: -1,
            earlyTransfer: -1,
            investField: "债券混合",
            productType: 3103,
            activityTab: 0
        },
    """
    def __init__(self):
        super(CaifuJsonStorer,self).__init__()
        self.data_admin = DatabaseAdmin() 

    def load(self,json_str):
        super(CaifuJsonStorer,self).load(json_str)
        self.object_ = self.object_['data']['list']
        #print self.object_.keys()
        #self.object_ = [self.object_]

    def store(self):
        super(CaifuJsonStorer,self).store()
    
        class A():
            pass

        def data_convert(fund_info):
            data=A()
            data.title = fund_info['title']
            data.invest_cycle = fund_info['investCycle']
            data.expected_profit_rate = fund_info['expectedProfitRate']
            data.lowest_amount = fund_info['lowestAmount']
            data.risk_type = fund_info['risk']
            data.description = ''
            data.invest_type = fund_info['investField']

            return data

        
        converted_datas = map(data_convert,self.object_)
        self.data_admin.batch_insert(converted_datas)


class StorerManager(object):
    """ manage the storer """

    def __init__(self):
        self._storer={}
        self._storer['json']=CaifuJsonStorer
        

    def get_storer(self,type_str):
        return self._storer[type_str]


def test_caifu_json_storer():
    caifu_storer = CaifuJsonStorer()
    jstr =u'{"titleAll":"\u9646\u91d1\u6240 - \u7a33\u76c8-\u5b89e\u8d37","id":345775725,"title":"\u9646\u91d1\u6240 - \u7a33\u76c8-\u5b89e\u8d37","investCycle":"36\u4e2a\u6708","expectedProfitRate":"8.40","expectedProfitName":"\u9884\u671f\u6536\u76ca\u7387","lowestAmount":"1\u4e07","breakEven":1,"supplierLogoUrl":"http:\/\/t12.baidu.com\/it\/u=2263955969,2201160335&fm=58","rawUrl":"http:\/\/www.baidu.com\/zhixin.php?url=X0TK00aPP8-7H5_wbCxCqtb5T2XYBgc242PodVHMuLnX9Ya3wSfTeRA76CCZpnijoWwEqrfhBbS_al27Gy8Tn5i2YD3KAny7geMg49AMykQZpLoP5K2XQtnufxKE1Hm8UzdaNDR.7Y_iR2qvHzu8WudFwKfh96CHnsq_Zw4pMlSennQ7IMWYvIg3hnx-gbtXr1xEvuQQQQQQQQQ5QVtoSVZZJmJCRnXZB19zptrHy1odECEuCFtJrjy1Ycs4PUnPKeC_5Zy193LultrHGCmXPvxIug9zIQ_Ijjk8LmTMHZ93LI5u3qhZFYYTXZWINfCm3t57Mjbd3ThEKAhUzC3PU5fWjbd3cY1mTh8PrM5nMRSr6hUoG8molOfhuBEze7BmOQOwOQ4ojPakggb3d6.THY0IZF9uANGujYvrH0k0APETLwxTh78p1Ys0ZPdmM7GujYkn1bdrHcvrHb4nHDkPWf10ANYXgK-5HDk0ANGujYs0ANbugPWpyfqn0KWUA-WpdqYXgK-5Hc0TA3qnfKYugFVpy49UjYs0ZGY5gP-UAm0TZ0q0AN3Ijd9mvP-TLPRXgK-rhdsr1VsIh-brW0erv-1N-PfrW0erLKzUvwdmLwRXgK-rWnknjc0mLFW5HRvPjDv","risk":"\u6fc0\u8fdb\u578b","profitType":0,"profitDesc":[{"name":"\u6536\u76ca\u8bf4\u660e","value":"\u4e0d\u540c\u4ea7\u54c1\u7684\u9884\u671f\u5e74\u5316\u6536\u76ca\u7387\u7684\u542b\u4e49\u6709\u6240\u533a\u522b\uff0c\u4ea7\u54c1\u6258\u7ba1\u8d39\u7387\u548c\u624b\u7eed\u8d39\u8bf7\u54a8\u8be2\u5177\u4f53\u53d1\u552e\u673a\u6784\u3002"}],"earlyBack":0,"earlyTransfer":1,"investField":"\u4fe1\u8d37\u8d44\u4ea7","productType":"3102","idea":"\u5e73\u5b89\u62c5\u4fdd\u516c\u53f8\u5168\u989d\u672c\u606f\u62c5\u4fdd\uff0c\u671f\u9650\u7075\u6d3b\uff0c60\u5929\u5373\u53ef\u8f6c\u8ba9","channelUrl":"http:\/\/www.baidu.com\/zhixin.php?url=X0TK00KNGYi7isajfuA5rYT_v2TIsJP5ecbXWUVrelWP2zDtBLdf0Qyvx74v60aTolg0WjqQYMU5rntN_stAXQOvAoH-Was6pvhcVGtz2W4C8-XrYsr8SKRqPD78ckgOSPs4i5Y.DY_aW6d_qa7BmOQOwOQ4ojPakblX1Iv0.THY0IZF9uANGujYvrH0k0APETLwxTh78p1Ys0ZPdmM7GujYkn1bdrHcvrHb4nHDkPWf10ANYXgK-5HDk0ANGujYs0ANbugPWpyfqn0KWUA-WpdqYXgK-5Hc0TA3qnfKYugFVpy49UjYs0ZGY5gP-UAm0TZ0q0AN3Ijd9mvP-TLPRXgK-rhdsr1VsIh-brW0erv-1N-PfrW0erLKzUvwdmLwRXgK-rWnknjc0mLFW5HRvPjDv","channelName":"\u4e2d\u56fd\u5e73\u5b89\u4eba\u5bff\u4fdd\u9669\u80a1\u4efd\u6709\u9650\u516c\u53f8","channelNameShort":"\u5e73\u5b89\u9646\u91d1\u6240","activityTab":0}'  
    caifu_storer.load(jstr)
    caifu_storer.store()
    

def test_storer_manager():
    
    sm = StorerManager()
    print type(sm.get_storer('json')())

if __name__ == '__main__':
    test_caifu_json_storer()
