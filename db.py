# -*- coding:utf-8 -*-
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column
from sqlalchemy.types import CHAR,Integer,String,Unicode
from sqlalchemy.ext.declarative  import declarative_base

from config import db_engine_url

    # Below to make the config flexible:
    # db_config = {
    #     'host' : '127.0.0.1',
    #     'user' : 'root',
    #     'passwd':'passw0rd',
    #     'db'   : 'spider',
    #     'charset':'utf8'
    # }
      
      
    # engine = create_engine('mysql://%s:%s@%s/%s?charset=%s'%(db_config['user'],db_config['passwd'],db_config['host'],db_config['db'],db_config['charset']),echo=True)

_engine = create_engine(db_engine_url,encoding='utf8')
_session = None
_Base = declarative_base()


class Funds(_Base):
    __tablename__ = 'funds'
    __table_args__ = {
        'mysql_engine':'InnoDB',
        'mysql_charset':'utf8'
    }

    id = Column('id',Integer,primary_key=True)
    title = Column('title',Unicode(100))
    invest_cycle = Column('invest_cycle',Unicode(30))
    expected_profit_rate = Column('expected_profit_rate',Unicode(10))
    lowest_amount = Column('lowest_amount',Unicode(10))
    risk_type = Column('risk_type',Unicode(10))
    description = Column('description',Unicode(1000))
    invest_type = Column('invest_type',Unicode(10))


def init_db():
    global _engine
    global _Base
    _Base.metadata.create_all(_engine)


def destroy_db():
    global _engine
    global _Base
    _Base.metadata.drop_all(_engine)


def test_insert():
    global _engine
    global _session

    if not _session:
        db_session = sessionmaker(bind=_engine)
        _session = db_session()

    fund_test = Funds(title=u'建信基金 - 坚信双息红利债券',
                    invest_cycle=u'一个月以下',
                    expected_profit_rate=u'0.36',
                    lowest_amount=u'一个月以下',
                    risk_type=u'稳健型',
                    description=u'具体请咨询相关机构（you suck!）',
                    invest_type=u'债券混合') 

    _session.add(fund_test)
    _session.commit()


def commit(func):
    def wrapper(*args,**kv):
        func(*args,**kv)
        assert _session
        _session.commit()

    return wrapper


class DatabaseAdmin():

    def __init__(self):
        global _session
        global _engine
        if not _session:
            db_session = sessionmaker(bind=_engine)
            _session = db_session()

        
    def single_insert(self,data):
        """ invode the batch insert to reuse code."""

        batch_insert([data])



    @commit
    def batch_insert(self,data_iter):
        """ iterate over data set and insert into database"""

        for data in data_iter:
            fund = Funds(title=data.title,
                            invest_cycle=data.invest_cycle,
                            expected_profit_rate=data.expected_profit_rate,
                            lowest_amount=data.lowest_amount,
                            risk_type=data.risk_type,
                            description=data.description,
                            invest_type=data.invest_type) 
            _session.add(fund)
    
    @classmethod
    def init_db(cls):
        _Base.metadata.create_all(_engine)

    @classmethod
    def destroy_db(cls):
        _Base.metadata.drop_all(_engine)


def test_database_admin():
    data_admin = DatabaseAdmin()
    class A():
        pass

    a=A()
    a.title=u'建信基金 - 坚信双息红利债券'
    a.invest_cycle=u'一个月以下'
    a.expected_profit_rate=u'0.36'
    a.lowest_amount=u'一个月以下'
    a.risk_type=u'稳健型'
    a.description=u'具体请咨询相关机构（you suck!）'
    a.invest_type=u'债券混合' 
    data_admin.batch_insert([a])
    

if __name__ == '__main__':
    #init_db()
    test_insert()
    #test_database_admin()
