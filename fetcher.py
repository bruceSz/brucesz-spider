import json
import sys

import eventlet
from eventlet.green import urllib2
import config
import storer


''' to utilize the eventlet lib,raise stopIteration exception in the baidu suffix generator
    if not using it, we can delay the stop condition until we have fetched some content match end condition.
    
'''

def fetch(url):
    req = urllib2.Request(url)
    return urllib2.urlopen(req).read().decode('unicode_escape')


def multi_fetch(urls):
    # TODO: The ret has fixed type here,should avoid it in the future.
    #       currently just stay where we are now.
    ret = []
    for url in urls:
        ret.append(fetch(url))

    return ret

def multi_fetcher(urls):
    pool = eventlet.GreenPool()
    return pool.imap(fetch,urls)
   
def urls_generator(baseurl,change_seq):
    for change in change_seq:
        yield baseurl+change



# Here are the pipeline of the whole process:
# 1: The urls_generator will generate iterable urls
# 2: The multi_fetcher will leverage the parameter urls,
#    which is also a iterable,and apply fetch on it.
#   
    
def infinite_num_seq(start=0):
    for i in xrange(start,sys.maxint):
        yield i

def url_suffix_seq():
    ''' use ininite num seq (the generator) to generate a seq of baidu caifu url suffix '''

    inseq = infinite_num_seq(start=1)
    for num in inseq:
        yield 'pageSize='+config.pageSize+'&pageNum='+str(num)


def baidu_url_suffix_seq():
    ''' the suffix_seq generate baidu caifu funds data in form of "pageSize=10&pageNum=87" '''
    
    sseq = url_suffix_seq()
    for suffix in sseq:
        if suffix.endswith(config.pageNum):
            raise StopIteration
        yield suffix

            


def test_multi_fetcher():

    baseurl = 'http://caifu.baidu.com/wealth/ajax?'
    suffixs = ['pageSize=10&pageNum=1']
    ug = urls_generator(baseurl,suffixs)
    contents = multi_fetcher(ug)
    for content in contents:
        print content

def test_infinite_seq():
    iseq = infinite_num_seq()
    for i in iseq:
        if i > 10:
            break
        print i
        
    
def baidu_caifu_data_fetchor():
    sm = storer.StorerManager()
    json_storer = sm.get_storer('json')
    jstorer = json_storer()

    
    baseurl = config.baseurl
    suffixs = baidu_url_suffix_seq()
    ug = urls_generator(baseurl,suffixs)
    contents = multi_fetcher(ug)
    for content in contents:
        print content
        jstorer.load(content)
        jstorer.store()


def test_url_suffix_seq():
    sseq = url_suffix_seq()
    for suffix in sseq:
        if suffix.endswith('10'):
            break
        print suffix


def test_baidu_url_suffix_seq():
    baidu_suffixs = baidu_url_suffix_seq()
    for bs in baidu_suffixs:
        print bs

if __name__ == '__main__':
    #test_multi_fetcher()
    #test_infinite_seq()
    #test_url_suffix_seq()
    #test_baidu_url_suffix_seq()
    baidu_caifu_data_fetchor()
